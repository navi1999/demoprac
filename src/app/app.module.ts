import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Comp1Component } from './comp1/comp1.component';
import { Comp2Component } from './comp2/comp2.component';
import { ParentpracComponent } from './parentprac/parentprac.component';
import { ChildpracComponent } from './parentprac/childprac/childprac.component';
import { RformspracComponent } from './rformsprac/rformsprac.component';
import {ReactiveFormsModule} from '@angular/forms';
import { TemplateformsComponent } from './templateforms/templateforms.component';
import {FormsModule} from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    Comp1Component,
    Comp2Component,
    ParentpracComponent,
    ChildpracComponent,
    RformspracComponent,
    TemplateformsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
