import { Component, OnInit } from '@angular/core';
import {SharedService} from '../shared.service';
@Component({
  selector: 'app-lazyload',
  templateUrl: './lazyload.component.html',
  styleUrls: ['./lazyload.component.scss']
})
export class LazyloadComponent implements OnInit {

  constructor(private shared1:SharedService) { }
  public movies:any[]=this.shared1.movieslist;
  ngOnInit(): void {
  }
  fun(value:any)
  {
    this.shared1.emit1<any>(value.name);
  }
}
