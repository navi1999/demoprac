import { Injectable } from '@angular/core';
import {BehaviorSubject,Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public names:any[]=['naveen','niharika','prudhvi','revanth'];
  constructor() { }
  public movieslist:any[]=[
    {name:'rrr',lead:'NTR'},
    {name:'pushpa',lead:'alluarjun'},
    {name:'kgf2',lead:'yash'},
    {name:'charlie',lead:'dulquar'}
  ];
  public subject=new BehaviorSubject<any>('');
  emit<T>(data:any)
  {
    this.subject.next(data);
  }
  on<T>(): Observable<T>{
    return this.subject.asObservable();
  }
  public subject1=new BehaviorSubject<any>('');
  emit1<T>(data1:any)
  {
    this.subject1.next(data1);
    console.log(this.subject1.value);
  }
  on1<T>(): Observable<T>{
    return this.subject1.asObservable();
  }
}
