import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-childprac',
  templateUrl: './childprac.component.html',
  styleUrls: ['./childprac.component.scss']
})
export class ChildpracComponent implements OnInit {
  @Input() data:any='';
  @Output() output=new EventEmitter<any>();
  constructor() { }
  senddata()
  {
    this.output.emit('This is data from child');
  }
  ngOnInit(): void {
  }

}
