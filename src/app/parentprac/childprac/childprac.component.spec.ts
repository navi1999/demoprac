import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildpracComponent } from './childprac.component';

describe('ChildpracComponent', () => {
  let component: ChildpracComponent;
  let fixture: ComponentFixture<ChildpracComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildpracComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildpracComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
