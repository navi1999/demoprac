import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentpracComponent } from './parentprac.component';

describe('ParentpracComponent', () => {
  let component: ParentpracComponent;
  let fixture: ComponentFixture<ParentpracComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParentpracComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentpracComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
