import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parentprac',
  templateUrl: './parentprac.component.html',
  styleUrls: ['./parentprac.component.scss']
})
export class ParentpracComponent implements OnInit {

  constructor() { }
  public value:any='';
  receivemessage($event:any)
  {
    console.log($event);
    this.value=$event;
  }
  ngOnInit(): void {
  }

}
