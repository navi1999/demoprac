import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Comp1Component } from './comp1/comp1.component';
import { Comp2Component } from './comp2/comp2.component';
import { ParentpracComponent } from './parentprac/parentprac.component';
import { RformspracComponent } from './rformsprac/rformsprac.component';
import { TemplateformsComponent } from './templateforms/templateforms.component';

const routes: Routes = [
  {
   path:'template',
   component:TemplateformsComponent
  },
  {
    path:'comp1',
    component:Comp1Component
  },
  {
    path:'comp2',
    component:Comp2Component
  },
  {
    path:'parentprac',
    component:ParentpracComponent
  },
  {
    path:'rforms',
    component:RformspracComponent
  },
  { path: 'lazyload', loadChildren: () => import('./lazyload/lazyload.module').then(m => m.LazyloadModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
