import { Component, OnInit } from '@angular/core';
import {SharedService} from '../shared.service';
@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.scss']
})
export class Comp1Component implements OnInit {

  constructor(private shared1:SharedService) { }
  public names:any[]=this.shared1.names;
  public movieslist:any[]=this.shared1.movieslist;
  ngOnInit(): void {
  }
  sendmessage(j:any)
  {
    this.shared1.emit<any>(j);
  }
}
