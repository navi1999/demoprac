import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
@Component({
  selector: 'app-comp2',
  templateUrl: './comp2.component.html',
  styleUrls: ['./comp2.component.scss']
})
export class Comp2Component implements OnInit {

  constructor(private shared1:SharedService) { }
  public indexvalue:any='';
  public movieslist:any[]=[];
  ngOnInit(): void {
    this.shared1.on<any>().subscribe(
      data=>{
        this.indexvalue=data;
      }
    );
    this.movieslist=this.shared1.movieslist;
  }

}
