import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup,Validators } from '@angular/forms';

@Component({
  selector: 'app-rformsprac',
  templateUrl: './rformsprac.component.html',
  styleUrls: ['./rformsprac.component.scss']
})
export class RformspracComponent implements OnInit {

  constructor() { }
  
  ngOnInit(): void {
  }
  public myform=new FormGroup({
  'name':new FormControl('',[Validators.required,Validators.minLength(1)]),
  'email':new FormControl('',[Validators.minLength(2)])
  });
  public formslist:any[]=[];
  displayform()
  {
    console.log(this.myform.value);
    this.formslist.push(this.myform.value);
    this.myform.reset();
  }
}
