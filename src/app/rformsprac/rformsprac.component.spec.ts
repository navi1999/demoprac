import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RformspracComponent } from './rformsprac.component';

describe('RformspracComponent', () => {
  let component: RformspracComponent;
  let fixture: ComponentFixture<RformspracComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RformspracComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RformspracComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
